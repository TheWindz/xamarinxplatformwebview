﻿using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


// TODO:
// handle no internet connection error
namespace Users
{
	public partial class MainPage : ContentPage
	{
        public const String URL_USERS = "http://app12u12frontendnodejs.us-east-2.elasticbeanstalk.com";
        //public const String URL_USERS = "https://app.12u12.com";
        //public const String URL_USERS = "https://tut-test-app.herokuapp.com";
        //public const String URL_USERS = "https://test.webrtc.org/";

        public const String FRONTEND_FUNCTION_NAME_SETFROMMOBILE = "setIsLoadedFromMobile()";

        private MyWebView m_webView;

        public MainPage()
		{
            InitializeComponent();
            //initPermissions();
            Task<bool> b = initPer();
            System.Diagnostics.Debug.Write("permissions============================================================" + b.Result);
            initView();
		}

        private async Task<bool> initPer()
        {
            var statusCamera = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            var statusMicrophone = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Microphone);

            return /*statusCamera == PermissionStatus.Granted &&*/ statusMicrophone == PermissionStatus.Granted;
        }

        /*private async void initPermissions()
        {
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Microphone);
            if ( status != PermissionStatus.Granted )
            {
                if ( await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Microphone))
                {
                    await DisplayAlert("Need Microphone","Gonna need that microphone", "OK");
                }

                var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Microphone);

                // Best practice to always check that the key exists
                if (results.ContainsKey(Permission.Camera))
                    status = results[Permission.Camera];
            }

            if(status==PermissionStatus.Granted)
            {

            }
            else if (status != PermissionStatus.Granted)
            {
                await DisplayAlert("Microphone Denied","Can't continue, try again", "OK");
            }
        }*/

        private void OnNavigatedHandler(object p_sender, WebNavigatedEventArgs p_args)
        {
            // this will call function in the frontend website
            m_webView.Eval(FRONTEND_FUNCTION_NAME_SETFROMMOBILE);

            /*Console.WriteLine("Navigated to:" + p_args.Url);
            m_currSubPage = p_args.Url;
            //Console.WriteLine("m_currSubPage:" + m_currSubPage);
            if (m_currSubPage == (URL_USERS + "/"))
            {
                Console.WriteLine("Change Page");
                m_currSubPage = (URL_USERS + URL_SUBPAGE_DASHBOARD);
            }*/
        }

        private void initView()
        {
            m_webView = new MyWebView();
            m_webView.Source = URL_USERS;
            Content = m_webView;

            m_webView.Navigated += OnNavigatedHandler;
        }
    }
}
